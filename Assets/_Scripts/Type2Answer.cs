﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//THIS SCRIPT GOES INTO EVERY TYPE 2 SCREEN GAMEOBJECT

public class Type2Answer : GenericAnswer
{
    //The list of selected options
    public List<GameObject> SelectedOptions = new List<GameObject>();

    //The answers chosen by the user and the correct answers
    public List<string>
        ChosenAnswers,
        ExpectedAnswers;

    //The maximum number of selected answers at once
    public byte MaxSelectedAnswers = 3;

    new public void ConfirmAnswer()
    {
        //Check if there is any answer selected before confirming
        if (SelectedOptions.Count > 0)
        {
            transform.GetChild(3).GetComponent<Button>().interactable = false;
            
            foreach(GameObject option in Options)
            {
                option.GetComponent<Button>().interactable = false;
                option.GetComponent<Animator>().SetBool("QuestionAnswered", true);
            }

            foreach(GameObject opt in SelectedOptions)
            {
                Button button = opt.GetComponent<Button>();
                ColorBlock buttonCB = button.colors;
                buttonCB.disabledColor = AppColours.ButtonSelectedColour;
                button.colors = buttonCB;
            }

            transform.GetChild(3).GetComponent<Animator>().SetBool("QuestionAnswered", true);
            transform.GetChild(1).GetChild(0).GetComponent<Animator>().SetBool("QuestionAnswered", true);

            Debug.Log("There are selected options. Confirming choice.");

            //Add all the selected options' text to the chosen answers list
            foreach (GameObject answerObject in SelectedOptions)
            {
                ChosenAnswers.Add(answerObject.GetComponentInChildren<Text>().text);
            }

            //Add a new type 2 screen to the stats
            ConfirmNewType2Struct();

            Invoke("ShowNextScreen", 1f);
        }
        else
        {
            Debug.Log("No selected options.");
        }
    }

    private void ConfirmNewType2Struct()
    {
        //Get the image's name
        string spriteName = transform.GetChild(1).GetChild(0).GetComponent<Image>().sprite.name;

        //Get all the chosen answers into an array
        string[] answersArray = ChosenAnswers.ToArray();

        _stats.CreateType2(spriteName, (byte) SelectedOptions.Count, answersArray);
    }

    new void OnEnable()
    {
        //Reset both the selected options and chosen answers
        SelectedOptions = new List<GameObject>();
        ChosenAnswers = new List<string>();

        //Reset all button's colours
        foreach(GameObject option in Options)
        {
            option.GetComponent<Type2OptionConfirmation>().ResetButtonColours();
        }

        //Reactivate the options
        StartCoroutine(ActivateOptions());
    }
}
