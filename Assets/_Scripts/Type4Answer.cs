﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

//THIS SCRIPT GOES INTO EVERY TYPE 4 SCREEN GAMEOBJECT

public class Type4Answer : GenericAnswer
{
    //The selected answer
    public GameObject SelectedOption;

    //The time until the options show up on screen
    public float _timeToActivateAdvanceButton = 3f;

    new private void Awake()
    {
        //Instance creation
        _stats = transform.parent.GetComponent<StatsRegister>();

        //Reset option GameObjects
        Options = new GameObject[OptionNumber];
        //Fill it with the option buttons
        for (int i = 0; i < OptionNumber; i++)
        {
            Options[i] = transform.GetChild(2).GetChild(i).gameObject;
        }
        
        StartCoroutine(ActivateAdvanceButton());
    }

    protected IEnumerator ActivateAdvanceButton()
    {
        //Deactivate the advance button (to avoid inconsistency when the back button is pressed)
        ToggleObjectByChildIndex(3, false);

        //Wait x seconds before continuing
        yield return new WaitForSecondsRealtime(_timeToActivateAdvanceButton);

        if (!transform.GetChild(3).GetComponent<Animator>().GetBool("QuestionAnswered"))
        {
            //Reactivate the advance button and its interactivity
            ToggleObjectByChildIndex(3, true);
        }
    }

    new public void ConfirmAnswer()
    {
        if(SelectedOption != null)
        {
            //Add the two emotions (sorted by preference) to the stats
            _stats.AddType4(SelectedOption.GetComponent<Type4Option>().ThisAnswer.ToLower(), SelectedOption.GetComponent<Type4Option>().OtherAnswer.ThisAnswer.ToLower());

            transform.GetChild(3).GetComponent<Button>().interactable = false;

            //Set the advance button to fade out
            transform.GetChild(3).GetComponent<Animator>().SetBool("QuestionAnswered", true);

            //Set both images to fade out
            transform.GetChild(1).GetChild(0).GetChild(0).GetComponent<Animator>().SetBool("QuestionAnswered", true);
            transform.GetChild(1).GetChild(1).GetChild(0).GetComponent<Animator>().SetBool("QuestionAnswered", true);

            //Hide this screen and show the next one
            Invoke("ShowNextScreen", 1f);
        }
        else
        {
            Debug.Log("No option selected. Please select an option before advancing.");
        }
    }

    new void OnEnable()
    {
        //Reset the selected option
        SelectedOption = null;

        //Reactivate the advance button
        StartCoroutine(ActivateAdvanceButton());
    }
}
