﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Type3Option : MonoBehaviour,
    //These interfaces allow control over user drags
    IBeginDragHandler, IDragHandler, IEndDragHandler
{
    //This GameObject
    private static GameObject _buttonBeingDragged;

    //Used to get this answer to be received into the image
    public GameObject ImageSelected;

    [SerializeField]
    //The other options
    private List<GameObject> _otherOptions = new List<GameObject>();

    [SerializeField]
    //The stored positions for the initial and final position (allows reset if necessary)
    public Vector2
        _initialPosition,
        _finalPosition;

    //The parent answer script
    private Type3Answer _answerScript;

    //This answer's text
    private string _thisAnswer;

    private void Awake()
    {
        //Reset the initial position (so that we can reset on and off properly)
        _initialPosition = GetComponent<RectTransform>().localPosition;
    }

    private void Start()
    {
        //Value definition
        for(int i = 0; i < transform.parent.childCount; i++)
        {
            _otherOptions.Add(transform.parent.GetChild(i).gameObject);
        }
        _otherOptions.Remove(gameObject);
        _thisAnswer = transform.GetChild(0).GetComponent<Text>().text;
        
        //Instance creation
        _answerScript = transform.parent.parent.GetComponent<Type3Answer>();
    }

    #region Handle drag commands
    public void OnBeginDrag(PointerEventData eventData)
    {
        //Define this button as the one to drag
        _buttonBeingDragged = gameObject;
    }

    public void OnDrag(PointerEventData eventData)
    {
        //Entirely functional, uncomment if solution below is not working properly
        //transform.position = Input.mousePosition;
        //Smooth out drag movement
        transform.position = Vector3.Lerp(transform.position, Input.mousePosition, 0.5f);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        //Stop dragging this button
        _buttonBeingDragged = null;

        //Check if the button was dropped on top of an image
        CheckOption();
    }
    #endregion

    public void CheckOption()
    {
        //If the button was not dropped onto an image, snap it back to its original position
        if(ImageSelected == null)
        {
            //Reset this button's position
            ResetPosition();

            //Reset this button's colours
            ResetButtonColours();

            //Check if the given answers array contains this answer
            if (System.Array.IndexOf(_answerScript.AnswersGiven, _thisAnswer) != -1)
            {
                for(int i = 0; i < 3; i++)
                {
                    //Delete this answer if it is in the given answers array
                    if(_answerScript.AnswersGiven[i] == _thisAnswer)
                    {
                        _answerScript.AnswersGiven[i] = "";
                    }
                }
            }
        }
        //If the button was dropped onto an image, snap it near the target image
        else
        {
            _finalPosition = ImageSelected.transform.parent.GetComponent<RectTransform>().position + new Vector3(0f, -200f, 0f);
            
            //In every option, check if there was an image connected; if there was, reset its position to let this option take place
            foreach(GameObject option in _otherOptions)
            {
                if(ImageSelected == option.GetComponent<Type3Option>().ImageSelected)
                {
                    option.GetComponent<Type3Option>().ResetPosition();
                    option.GetComponent<Type3Option>().ResetButtonColours();
                }
            }
            GetComponent<RectTransform>().position = _finalPosition;
            
            //Check to see if the given answers array already contains this answer
            for(int i = 0; i < 3; i++)
            {
                //Delete this answer if it is in the given answers array
                if(_answerScript.AnswersGiven[i] == _thisAnswer)
                {
                    _answerScript.AnswersGiven[i] = "";
                }
            }
            //Set the item with the same index as the picture as the answer given
            _answerScript.AnswersGiven[ImageSelected.transform.parent.GetSiblingIndex()] = _thisAnswer;

            //Reset this button's colours to its original colour
            Button _button = GetComponent<Button>();
            ColorBlock cb = _button.colors;
            cb.normalColor = AppColours.ButtonSelectedColour;
            cb.selectedColor = AppColours.ButtonSelectedColour;
            cb.disabledColor = AppColours.ButtonSelectedColour;
            _button.colors = cb;
            GetComponentInChildren<Text>().color = Color.white;

            //Activate the advance button
            _answerScript.ToggleObjectByChildIndex(3, true);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        //If the button is in contact with an image, define the target image as the one in contact
        if(other.CompareTag("Image"))
        {
            ImageSelected = other.gameObject;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        //If the button is no longer in contact with an image, the target image is reset
        if (other.CompareTag("Image"))
        {
            ImageSelected = null;
        }
    }

    public void ResetPosition()
    {
        GetComponent<RectTransform>().localPosition = _initialPosition;
    }

    public void ResetButtonColours()
    {
        //Reset this button's colours to its original colour
        Button _button = GetComponent<Button>();
        ColorBlock cb = _button.colors;
        cb.normalColor = AppColours.ButtonNormalColour;
        cb.selectedColor = AppColours.ButtonNormalColour;
        cb.disabledColor = new Color(0.784f, 0.784f, 0.784f);
        _button.colors = cb;
        GetComponentInChildren<Text>().color = Color.black;
    }

    void OnEnable()
    {
        //Reset to the initial position
        ResetPosition();
        ResetButtonColours();
    }
}
