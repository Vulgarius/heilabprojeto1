﻿using UnityEngine;
using UnityEngine.UI;

//THIS SCRIPT GOES INTO EVERY OPTION IN THE TYPE 1 SCREEN

public class Type1OptionConfirmation : MonoBehaviour
{
    //This answer's text
    public string ThisAnswer;

    //The answer's script
    private Type1Answer _type1AnswerScript;

    //This answer's button
    private Button _button;

    private void Start()
    {
        //Set the answer's text as the current text
        ThisAnswer = GetComponentInChildren<Text>().text;

        //Instance creation
        _type1AnswerScript = transform.parent.parent.GetComponent<Type1Answer>();
        _button = GetComponent<Button>();
    }

    public void ConfirmOption()
    {
        //Check if the answer is not currently selected
        if (_type1AnswerScript.SelectedAnswer != ThisAnswer)
        {
            foreach(GameObject option in _type1AnswerScript.Options)
            {
                //Search for the currently selected answer
                if(_type1AnswerScript.SelectedAnswer == option.GetComponent<Type1OptionConfirmation>().ThisAnswer)
                {
                    //Reset the previously selected button's colour to the original colour
                    ColorBlock currentAnswerCB = option.GetComponent<Button>().colors;
                    currentAnswerCB.normalColor = AppColours.ButtonNormalColour;
                    currentAnswerCB.selectedColor = AppColours.ButtonNormalColour;
                    option.GetComponent<Button>().colors = currentAnswerCB;
                    option.GetComponentInChildren<Text>().color = Color.black;
                }
            }

            //Select the option and change its button colours
            _type1AnswerScript.SelectedAnswer = ThisAnswer;
            ColorBlock cb = _button.colors;
            cb.normalColor = AppColours.ButtonSelectedColour;
            cb.selectedColor = AppColours.ButtonSelectedColour;
            _button.colors = cb;
            GetComponentInChildren<Text>().color = Color.white;
        }
        else
        {
            //Remove the option and change its button colours back to default
            _type1AnswerScript.SelectedAnswer = "";
            ColorBlock cb = _button.colors;
            cb.normalColor = AppColours.ButtonNormalColour;
            cb.selectedColor = AppColours.ButtonNormalColour;
            _button.colors = cb;
            GetComponentInChildren<Text>().color = Color.black;
        }
    }

    public void ResetButtonColours()
    {
        //Reset the button's colours to its original colour
        _button = GetComponent<Button>();
        ColorBlock cb = _button.colors;
        cb.normalColor = AppColours.ButtonNormalColour;
        cb.selectedColor = AppColours.ButtonNormalColour;
        cb.disabledColor = new Color(0.784f, 0.784f, 0.784f);
        _button.colors = cb;
        GetComponentInChildren<Text>().color = Color.black;
    }

    void OnEnable()
    {
        //Reset the button's colours to its original colour
        _button = GetComponent<Button>();
        ResetButtonColours();
    }
}
