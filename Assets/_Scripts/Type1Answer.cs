﻿using UnityEngine;
using UnityEngine.UI;

//THIS SCRIPT GOES INTO EVERY TYPE 1 SCREEN GAMEOBJECT

public class Type1Answer : GenericAnswer
{
    //The answer given by the user and the correct answer
    public string
        UserAnswer,
        CorrectAnswer;
    
    //Check if the answer given by the user is correct
    public bool IsCorrect;

    //The answer selected by the user (used for confirmation)
    public string SelectedAnswer;

    new public void ConfirmAnswer()
    {
        //Deactivate every option's interaction and set it to fade out
        foreach(GameObject option in Options)
        {
            option.GetComponent<Button>().interactable = false;
            option.GetComponent<Animator>().SetBool("QuestionAnswered", true);
        }

        //Set the advance button to fade out
        transform.GetChild(3).GetComponent<Animator>().SetBool("QuestionAnswered", true);
        //Set the image to fade out
        transform.GetChild(1).GetChild(0).GetComponent<Animator>().SetBool("QuestionAnswered", true);

        //Set the user's answer as the answer received
        UserAnswer = SelectedAnswer;

        //If the user answered correctly
        if(UserAnswer == CorrectAnswer)
        {
            IsCorrect = true;
            Debug.Log("The answer is correct!");

            //Increase the stats' correct answers
            _stats.CorrectT1++;

            GameObject.FindGameObjectWithTag("ScreenManager").GetComponent<ScreenManager>().RemoveType1Reinforcement(gameObject);
        }
        //If the user answered incorrectly
        else
        {
            IsCorrect = false;
            Debug.Log("The answer is incorrect.");
        }

        Invoke("ShowNextScreen", 1f);
    }

    public void CheckAnswerConfirm()
    {
        //Check if the answer has something
        if(SelectedAnswer != "")
        {
            ConfirmAnswer();

            //Deactivate the advance button
            transform.GetChild(3).GetComponent<Button>().interactable = false;

            foreach(GameObject option in Options)
            {
                //Check if this is the option selected
                if(option.GetComponent<Type1OptionConfirmation>().ThisAnswer == SelectedAnswer)
                {
                    //Set this button's disabled colour as grey
                    Button button = option.GetComponent<Button>();
                    ColorBlock buttonCB = button.colors;
                    buttonCB.disabledColor = AppColours.ButtonSelectedColour;
                    button.colors = buttonCB;
                }
            }
        }
        else
        {
            Debug.Log("The answer is empty. Please select an answer.");
        }
    }

    new protected void OnEnable()
    {
        //Reset the selected answer (to prevent re-pressing advance button immediately)
        SelectedAnswer = "";

        //Reactivate the options
        StartCoroutine(ActivateOptions());
    }
}
