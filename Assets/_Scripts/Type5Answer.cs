﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//THIS SCRIPT GOES INTO EVERY TYPE 5 SCREEN GAMEOBJECT

public class Type5Answer : MonoBehaviour
{
    //The stats register script (to register the answer)
    protected StatsRegister _stats;
    
    //The time until the options show up on screen
    public float _timeToActivateAdvanceButton = 3f;

    //Every option button's GameObject
    public GameObject[] Options;

    //The currently selected answer
    public GameObject CurrentlySelectedAnswer;

    //The given answers (even is the first selected, odd is the second selected)
    public List<string>
        AllAnswers = new List<string>(),
        MatchedAnswers = new List<string>();

    // Start is called before the first frame update
    protected void Awake()
    {
        //Instance creation
        _stats = transform.parent.GetComponent<StatsRegister>();

        //Reset option GameObjects
        Options = new GameObject[6];
        //Fill it with the option buttons
        for (int i = 0; i < 6; i++)
        {
            Options[i] = transform.GetChild(1).GetChild(i).gameObject;
            AllAnswers.Add(Options[i].GetComponentInChildren<Type5Option>().Answer);
        }

        StartCoroutine(ActivateAdvanceButton());
    }

    protected IEnumerator ActivateAdvanceButton()
    {
        //Activate the advance button (to avoid inconsistency when the back button is pressed)
        ToggleObjectByChildIndex(2, false);

        //Wait x seconds before continuing
        yield return new WaitForSecondsRealtime(_timeToActivateAdvanceButton);

        if (!transform.GetChild(2).GetComponent<Animator>().GetBool("QuestionAnswered"))
        {
            //Activate the advance button
            ToggleObjectByChildIndex(2, true);
        }
    }

    public void ConfirmAnswer()
    {
        //Make sure the answers list is not empty
        if(MatchedAnswers.Count > 1)
        {
            transform.GetChild(2).GetComponent<Button>().interactable = false;

            //Set the advance button to fade
            transform.GetChild(2).GetComponent<Animator>().SetBool("QuestionAnswered", true);

            for(int i = 0; i < 6; i++)
            {
                //Set each image to fade out
                transform.GetChild(1).GetChild(i).GetComponent<Animator>().SetBool("QuestionAnswered", true);
            }

            _stats.CreateType5(MatchedAnswers, AllAnswers);

            Invoke("ShowNextScreen", 1.5f);
        }
    }

    public void MatchAnswers(GameObject firstOption, GameObject secondOption)
    {
        CurrentlySelectedAnswer = null;

        //Add the options' text to the matched answers list
        MatchedAnswers.Add(firstOption.GetComponentInChildren<Type5Option>().Answer);
        MatchedAnswers.Add(secondOption.GetComponentInChildren<Type5Option>().Answer);
        AllAnswers.Remove(firstOption.GetComponentInChildren<Type5Option>().Answer);
        AllAnswers.Remove(secondOption.GetComponentInChildren<Type5Option>().Answer);

        //Disable their interaction
        firstOption.GetComponentInChildren<Button>().interactable = false;
        secondOption.GetComponentInChildren<Button>().interactable = false;

        //Change the image's colour to indicate that it cannot be selected
        StartCoroutine(ShowSelectedAnswer(firstOption));
        StartCoroutine(ShowSelectedAnswer(secondOption));
    }

    private IEnumerator ShowSelectedAnswer(GameObject option)
    {
        //Set the colour to the selected colour
        option.GetComponent<Image>().color = AppColours.ButtonAlt2SelectedColour;

        //Wait a bit
        yield return new WaitForSeconds(0.5f);

        //Set the colour to a non-interactable colour
        option.GetComponent<Image>().color = new Color(0.2f, 0.2f, 0.2f);
    }

    public void ToggleObjectByChildIndex(int childIndex, bool activationBool)
    {
        //Activate or deactivate the target child of this object
        transform.GetChild(childIndex).gameObject.SetActive(activationBool);

        //If the target object is a button, set it to interactable
        if (transform.GetChild(childIndex).GetComponent<Button>() != null)
        {
            transform.GetChild(childIndex).GetComponent<Button>().interactable = true;
        }
    }

    protected void ShowNextScreen()
    {
        //Hide this screen and show the next one
        GameObject.FindGameObjectWithTag("ScreenManager").GetComponent<ScreenManager>().ShowNextScreen();
        
        //Update the progress bar
        GameObject.FindGameObjectWithTag("ProgressBar").GetComponent<ProgressBarBehaviour>().UpdateProgressBar();
    }

    void OnEnable()
    {
        //Reset the screen
        ResetScreen();
    }

    public void ResetScreen()
    {
        //Reset the answer lists
        AllAnswers = new List<string>();
        MatchedAnswers = new List<string>();

        //Reset the currently selected answer
        CurrentlySelectedAnswer = null;

        for (int i = 0; i < 6; i++)
        {
            //Add the option to the all-answers list
            AllAnswers.Add(Options[i].GetComponentInChildren<Type5Option>().Answer);

            //Reset the button's colours to its original colours
            Options[i].GetComponentInChildren<Type5Option>().ResetButtonColours();

            //Reset the button's interaction
            Options[i].GetComponentInChildren<Button>().interactable = true;
        }

        StopCoroutine(ActivateAdvanceButton());
        StartCoroutine(ActivateAdvanceButton());
    }
}
