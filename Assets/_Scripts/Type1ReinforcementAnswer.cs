﻿using UnityEngine;
using UnityEngine.UI;

//THIS SCRIPT GOES INTO EVERY TYPE 1 SCREEN GAMEOBJECT

public class Type1ReinforcementAnswer : Type1Answer
{
    new public void ConfirmAnswer()
    {
        foreach(GameObject option in Options)
        {
            option.GetComponent<Button>().interactable = false;
            option.GetComponent<Animator>().SetBool("QuestionAnswered", true);
        }

        //Set the advance button to fade out
        transform.GetChild(3).GetComponent<Animator>().SetBool("QuestionAnswered", true);
        //Set the image to fade out
        transform.GetChild(1).GetChild(0).GetComponent<Animator>().SetBool("QuestionAnswered", true);

        //Set the user's answer as the answer received
        UserAnswer = SelectedAnswer;

        //If the user answered correctly
        if(UserAnswer == CorrectAnswer)
        {
            IsCorrect = true;
            Debug.Log("The answer is correct!");
        }
        //If the user answered incorrectly
        else
        {
            IsCorrect = false;
            Debug.Log("The answer is incorrect.");
        }

        Invoke("ShowNextScreen", 1f);
    }

    new public void CheckAnswerConfirm()
    {
        //Check if the answer has something
        if(SelectedAnswer != "")
        {
            ConfirmAnswer();

            //Deactivate the advance button
            transform.GetChild(3).GetComponent<Button>().interactable = false;

            foreach(GameObject option in Options)
            {
                //Check if this is the option selected
                if(option.GetComponent<Type1OptionConfirmation>().ThisAnswer == SelectedAnswer)
                {
                    //Set this button's disabled colour as grey
                    Button button = option.GetComponent<Button>();
                    ColorBlock buttonCB = button.colors;
                    buttonCB.disabledColor = AppColours.ButtonSelectedColour;
                    button.colors = buttonCB;
                }
            }
        }
        else
        {
            Debug.Log("The answer is empty. Please select an answer.");
        }
    }

    new void OnEnable()
    {
        //Reset the selected answer (to prevent re-pressing advance button immediately)
        SelectedAnswer = "";

        //Reactivate the options
        StartCoroutine(ActivateOptions());
    }
}
