﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenManager : MonoBehaviour
{
    //The screens present in the test
    public List<GameObject> _screens = new List<GameObject>();

    //The current screen index (to hide all others)
    public int
        CurrentScreenIndex,
        LastCorrectIndex;

    //The instruction text component
    private Text _instructions;

    //The place where the reinforcement screens go when the first one is correct
    private Transform
        _allScreens,
        _screenHolder;

    // Start is called before the first frame update
    void Start()
    {
        //Instance creation
        _allScreens = GameObject.FindGameObjectWithTag("Screens").transform;
        _screenHolder = GameObject.FindGameObjectWithTag("ScreenHolder").transform;
        for (int i = 0; i < _allScreens.childCount; i++)
        {
            _screens.Add(_allScreens.GetChild(i).gameObject);
        }
        
        //Value definition
        CurrentScreenIndex = 0;

        //Deactivate first screen of type's back button
        DeactivateFirstOfTypeBackButton();

        //Update the progress bar
        GameObject.FindGameObjectWithTag("ProgressBar").GetComponent<ProgressBarBehaviour>().UpdateProgressBar();
    }

    public void ShowNextScreen()
    {
        //Update current screen index in order to check which screen should be shown
        CurrentScreenIndex++;

        //Deactivate all screens
        foreach(GameObject screen in _screens)
        {
            screen.SetActive(false);
        }

        //Check if the user has reached the end of the test
        if(CurrentScreenIndex < _screens.Count)
        {
            //Activate the current index screen
            _screens[CurrentScreenIndex].SetActive(true);
        }
        else
        {
            //End the test and generate the report file
            _allScreens.GetComponent<StatsRegister>().EndTest();
        }
    }

    private void DeactivateFirstOfTypeBackButton()
    {
        //Deactivate the first screen of each type's back button
        DeactivateButton(0);
        DeactivateButton(24);
        DeactivateButton(31);
        DeactivateButton(36);
        DeactivateButton(38, 3);
    }

    private void DeactivateButton(int screenIndex)
    {
        //Deactivate the fourth child of the target object
        _screens[screenIndex].transform.GetChild(4).gameObject.SetActive(false);
    }

    private void DeactivateButton(int screenIndex, int childIndex)
    {
        //Deactivate the nth child of the target object (overload)
        _screens[screenIndex].transform.GetChild(childIndex).gameObject.SetActive(false);
    }

    public void RemoveType1Reinforcement(GameObject correctQuestion)
    {
        //Create a temporary list of all screens
        List<GameObject> tempScreens = new List<GameObject>();
        foreach(GameObject screen in _screens)
        {
            //Get the two screens with the same name
            if(screen.name == correctQuestion.name)
            {
                //Add the screen to the temporary list
                tempScreens.Add(screen);
                //Save the screen's index (for resetting, if necessary)
                LastCorrectIndex = _screens.IndexOf(screen);
            }
        }

        //Remove based on the index, because the first will be the original, the second will be the reinforcement
        tempScreens[1].transform.SetParent(_screenHolder);
        _screens.Remove(tempScreens[1]);
    }

    public void ResetType1Reinforcement()
    {
        //Get the last reinforcement screen to be put inside the screen holder
        Transform reinforcementToGet = _screenHolder.GetChild(_screenHolder.childCount - 1);

        //Reset the reinforcement screen's parent
        reinforcementToGet.SetParent(_allScreens);

        //Put it in its correct index, which was saved before
        reinforcementToGet.SetSiblingIndex(LastCorrectIndex);

        //Reinsert it in the screens list at its index
        _screens.Insert(LastCorrectIndex, reinforcementToGet.gameObject);
    }
}
