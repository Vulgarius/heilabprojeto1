﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class FileScreenManager : MonoBehaviour
{
    //The file text GameObject
    public GameObject FileText;

    //The file text's subdivisions
    private Text
        //The initial stats
        _patientName,
        _correctFraction,
        _correctPercentage,
        _date,
        _totalTime,
        _startHour,
        _endHour,

        //The question types
        _t1Stats,
        _t2Stats,
        _t3Stats,
        _t4Stats,
        _t5Stats;

    private void Awake()
    {
        //Instance creation
        #region Initial stats instancing
        _patientName = GameObject.FindGameObjectWithTag("PatientName").GetComponent<Text>();
        _correctFraction = GameObject.FindGameObjectWithTag("CorrectFraction").GetComponent<Text>();
        _correctPercentage = GameObject.FindGameObjectWithTag("CorrectPercentage").GetComponent<Text>();
        _date = GameObject.FindGameObjectWithTag("ReportDate").GetComponent<Text>();
        _totalTime = GameObject.FindGameObjectWithTag("TestTime").GetComponent<Text>();
        _startHour = GameObject.FindGameObjectWithTag("TestBegin").GetComponent<Text>();
        _endHour = GameObject.FindGameObjectWithTag("TestEnd").GetComponent<Text>();
        #endregion

        #region Question type instancing
        _t1Stats = GameObject.FindGameObjectWithTag("Type1Stats").GetComponent<Text>();
        _t2Stats = GameObject.FindGameObjectWithTag("Type2Stats").GetComponent<Text>();
        _t3Stats = GameObject.FindGameObjectWithTag("Type3Stats").GetComponent<Text>();
        _t4Stats = GameObject.FindGameObjectWithTag("Type4Stats").GetComponent<Text>();
        _t5Stats = GameObject.FindGameObjectWithTag("Type5Stats").GetComponent<Text>();
        #endregion
    }

    void OnEnable()
    {
        //Reset the scrollbar's value (bring the page back to the top of the text)
        GameObject.FindGameObjectWithTag("VerticalScrollbar").GetComponent<Scrollbar>().value = 1;
    }

    public void ShowFile(int fileIndex)
    {
        //Open the file, read it and close it
        StreamReader streamReader = new StreamReader(Application.persistentDataPath + "/relatorios/Relatorio"+ fileIndex);
        var fileContents = streamReader.ReadToEnd();
        streamReader.Close();
    
        //Create a temporary variable with the file's content
        var lines = fileContents;

        //Divide the file contents into the various details
        string[] textLines = lines.Split(';');

        //In each text subdivision, remove any line breaks
        for(int i = 0; i < 7; i++)
        {
            textLines[i] = textLines[i].Replace("\n", "");
        }

        //Set each stat as the text extracted
        _patientName.text = textLines[0];
        _correctFraction.text = textLines[1];
        _correctPercentage.text = textLines[2];
        _date.text = textLines[3];
        _totalTime.text = textLines[4];
        _startHour.text = textLines[5];
        _endHour.text = textLines[6];

        //Replace the rights and wrongs with blank and red circles
        textLines[7] = textLines[7].Replace("✓", " <color=00000000><size=24>" + "●" + "</size></color>");
        textLines[7] = textLines[7].Replace("✘", " <color=red><size=24>" + "●" + "</size></color>");

        //Set each question type's answers as the text extracted
        _t1Stats.text = textLines[7];
        _t2Stats.text = textLines[8];
        _t3Stats.text = textLines[9];
        _t4Stats.text = textLines[10];
        _t5Stats.text = textLines[11];

        for (int i = 0; i < 7; i++)
        {
            Debug.Log(textLines[i]);
        }
        
        //Reset the content size
        StartCoroutine("ResetContentSize");

        //Deactivate the file list screen
        GameObject.FindGameObjectWithTag("ReportScreen").SetActive(false);
    }

    private IEnumerator ResetContentSize()
    {
        ContentSizeFitter
            fileContentFitter = FileText.transform.parent.GetComponent<ContentSizeFitter>(),
            type1ContentFitter = FileText.transform.GetChild(1).GetChild(1).GetChild(1).GetComponent<ContentSizeFitter>();

        #region The messiest workaround in the history of the planet
        fileContentFitter.verticalFit = ContentSizeFitter.FitMode.MinSize;
        type1ContentFitter.enabled = false;
        yield return new WaitForSecondsRealtime(0.07f);
        fileContentFitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
        type1ContentFitter.enabled = true;

        yield return new WaitForEndOfFrame();
        type1ContentFitter.enabled = false;
        yield return new WaitForEndOfFrame();
        type1ContentFitter.enabled = true;

        yield return new WaitForEndOfFrame();
        type1ContentFitter.enabled = false;
        yield return new WaitForEndOfFrame();
        type1ContentFitter.enabled = true;
        #endregion
    }
}
