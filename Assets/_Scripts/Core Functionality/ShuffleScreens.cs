﻿using UnityEngine;

public class ShuffleScreens : MonoBehaviour
{
    //The type 1 screens
    private Transform[]
        Type1Screens,
        ReType1Screens,
        Type2Screens;

    //The stats register script
    private StatsRegister _stats;

    void Awake()
    {
        //Instance creation
        _stats = GetComponent<StatsRegister>();
        Type1Screens = new Transform[12];
        ReType1Screens = new Transform[12];
        Type2Screens = new Transform[7];
        _stats.Type1Answers = new Type1Answer[12];

        //Fill the type 1 screens array with the first 12 children
        for(int i = 0; i < 12; i++)
        {
            Type1Screens[i] = transform.GetChild(i);
            _stats.Type1Answers[i] = transform.GetChild(i).GetComponent<Type1Answer>();
        }

        //Fill the type 1 screens array with the second 12 children
        for(int i = 12; i < 24; i++)
        {
            ReType1Screens[i - 12] = transform.GetChild(i);
            _stats.ReType1Answers.Add(transform.GetChild(i).GetComponent<Type1Answer>());
        }

        //Fill the type 2 screens array with the third 12 children
        for (int i = 24; i < 31; i++)
        {
            Type2Screens[i - 24] = transform.GetChild(i);
        }

        //Shuffle the type 1 screens
        ShuffleType1Screens();

        transform.GetChild(0).gameObject.SetActive(true);
    }

    private void ShuffleType1Screens()
    {
        foreach(Transform type1Screen in Type1Screens)
        {
            //Generate a random index from 0 to 11
            int randIndex = Random.Range (0, Type1Screens.Length);
            
            //Set the random index as the sibling index to reorder
            type1Screen.SetSiblingIndex(randIndex);
        }

        foreach (Transform reType1Screen in ReType1Screens)
        {
            //Generate a random index from 0 to 11
            int randIndex = Random.Range(12, ReType1Screens.Length + 12);

            //Set the random index as the sibling index to reorder
            reType1Screen.SetSiblingIndex(randIndex);
        }

        foreach (Transform type2Screen in Type2Screens)
        {
            //Generate a random index from 0 to 11
            int randIndex = Random.Range(24, Type2Screens.Length + 24);

            //Set the random index as the sibling index to reorder
            type2Screen.SetSiblingIndex(randIndex);
        }
    }
}
