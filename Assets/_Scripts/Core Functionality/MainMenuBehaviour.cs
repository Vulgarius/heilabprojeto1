﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuBehaviour : MonoBehaviour
{
    //The scenes which will be loaded on click's index
    public int
        TestSceneIndex,
        CheckResultsSceneIndex;

    public void LoadMainMenuDestroyObject(GameObject ObjectToDestroy)
    {
        //Destroy the target object
        Destroy(ObjectToDestroy);

        //Load main menu
        SceneManager.LoadScene(0);
    }

    public void LoadNewTest()
    {
        //Load scene based on index
        SceneManager.LoadScene(TestSceneIndex);
    }

    public void LoadCheckResults()
    {
        //Load scene based on index
        SceneManager.LoadScene(CheckResultsSceneIndex);
    }
}
