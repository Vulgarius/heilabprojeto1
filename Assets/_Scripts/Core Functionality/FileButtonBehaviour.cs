﻿using UnityEngine;

public class FileButtonBehaviour : MonoBehaviour
{
    //The index of the button
    private int _index;

    public void StartShowFile()
    {
        //Show the file with the same index as the one in this button's name
        GameObject.FindGameObjectWithTag("ReportScreen").GetComponent<GetFilesReportScreen>().ShowFile(_index);
    }

    public void SetIndex(int i)
    {
        _index = i;
    }
}
