﻿using UnityEngine;

public class IDStringHolder : MonoBehaviour
{
    //The user's name
    public string UserID;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);   
    }
}
