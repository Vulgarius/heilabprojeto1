﻿using UnityEngine;
using UnityEngine.UI;

public class IDStringSpawner : MonoBehaviour
{
    //The object that holds the user's name
    public GameObject IDHolder;

    //The input field in which the user's name will be written
    private InputField _userIDInput;

    private GameObject
        _instance,
        _nextScreen;

    // Start is called before the first frame update
    void Awake()
    {
        //Instance creation
        _userIDInput = GameObject.FindGameObjectWithTag("UserInput").GetComponent<InputField>();
        _instance = GameObject.FindGameObjectWithTag("UserID");
        _nextScreen = GameObject.FindGameObjectWithTag("TestInstructionsScreen");

        //Deactivate the instructions screen
        _nextScreen.SetActive(false);
    }

    public void ConfirmUserID()
    {
        //Check if the user's name isn't empty and isn't filled with " "
        if(_userIDInput.text != "" && _userIDInput.text.Split(' ')[0] != "")
        {
            //Reset username
            _instance.GetComponent<IDStringHolder>().UserID = "";

            string tempUsername = _userIDInput.text;

            foreach(string s in tempUsername.Split(' '))
            {
                _instance.GetComponent<IDStringHolder>().UserID += s[0];
            }

            Debug.Log("The user's name is " + _instance.GetComponent<IDStringHolder>().UserID);

            //Switch between the user id input and the test instructions screen
            _nextScreen.SetActive(true);
            GameObject.FindGameObjectWithTag("UserIDScreen").SetActive(false);
        }
        else
        {
            Debug.Log("The user's name is invalid.");
        }
    }
}
