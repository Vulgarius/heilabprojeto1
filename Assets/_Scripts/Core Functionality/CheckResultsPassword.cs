﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CheckResultsPassword : MonoBehaviour
{
    //The password
    private string _password = "aaa";

    //The InputField in which the password attempt is
    private InputField _passwordInput;

    //The two screens (password and report)
    public GameObject
        PasswordScreen,
        ReportScreen;

    void Awake()
    {
        //Instance creation
        _passwordInput = GameObject.FindGameObjectWithTag("PasswordInput").GetComponent<InputField>();
    }

    public void CheckPassword()
    {
        //Check if the password is correct
        if(_passwordInput.text == _password)
        {
            Debug.Log("Password is correct!");

            //Switch between the password and report list screens
            PasswordScreen.SetActive(false);
            ReportScreen.SetActive(true);
        }
        //If it's not, send the user back to the main menu
        else
        {
            Debug.Log("Password is incorrect. Loading main menu...");
            LoadMainMenu();
        }
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene(0);
    }
}
