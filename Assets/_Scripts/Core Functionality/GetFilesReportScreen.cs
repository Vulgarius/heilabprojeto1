﻿using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class GetFilesReportScreen : MonoBehaviour
{
    //The path in which the report files are
    private string _path;

    //A list of all the names of report files
    private List<string> _fileNames = new List<string>();

    //The report file button object (for instancing)
    public GameObject FileButton;

    //The list object
    private Transform _listTransform;

    //The file screen
    public GameObject FileScreen;

    // Start is called before the first frame update
    void Awake()
    {
        //Instance creation
        _path = Application.persistentDataPath + "/relatorios/";
        _listTransform = GameObject.FindGameObjectWithTag("FileList").transform;

        //Try to get the report files
        GetAllFiles();
    }

    private void GetAllFiles()
    {
        //Check if the report directory already exists (if it doesn't, create it)
        if(Directory.Exists(Application.persistentDataPath + "/relatorios/"))
        {
            foreach (string file in System.IO.Directory.GetFiles(_path))
            {
                _fileNames.Add(file.Replace(_path + "Relatorio", ""));
                Debug.Log(file.Replace(_path, ""));
            }
            CreateList();
        }
        else
        {
            Debug.Log("No reports available.");
        }
    }

    private void CreateList()
    {
        //Create a new temporary number list
        List<int> tempNumber = new List<int>();
        //Parse the strings to integers to fill number list
        foreach(string fileName in _fileNames)
        {
            fileName.Replace("Relatorio", "");
            tempNumber.Add(int.Parse(fileName));
        }
        //Sort by number
        tempNumber.Sort();

        //Create buttons for each number in the index list
        foreach(int fileNumber in tempNumber)
        {
            GameObject buttonInstance = Instantiate(FileButton, _listTransform);
            buttonInstance.GetComponentInChildren<Text>().text = "Relatório " + fileNumber.ToString();
            buttonInstance.GetComponent<FileButtonBehaviour>().SetIndex(fileNumber);
        }
    }

    public void ShowFile(int index)
    {
        FileScreen.SetActive(true);
        FileScreen.GetComponent<FileScreenManager>().ShowFile(index);
    }
}
