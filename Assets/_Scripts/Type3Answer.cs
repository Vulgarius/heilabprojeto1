﻿using UnityEngine;
using UnityEngine.UI;

//THIS SCRIPT GOES INTO EVERY TYPE 3 SCREEN GAMEOBJECT

public class Type3Answer : GenericAnswer
{
    //The images used
    public Sprite[] Images = new Sprite[3];

    //The answers selected by the user
    public string[] AnswersGiven = new string[3];

    void Start()
    {
        //Set all the images to match the images given (in the Inspector)
        transform.GetChild(1).GetChild(0).GetChild(0).GetComponent<Image>().sprite = Images[0];
        transform.GetChild(1).GetChild(1).GetChild(0).GetComponent<Image>().sprite = Images[1];
        transform.GetChild(1).GetChild(2).GetChild(0).GetComponent<Image>().sprite = Images[2];
    }

    new public void ConfirmAnswer()
    {
        //Used to test if the answer array is empty
        byte emptyAnswers = 0;
        foreach(string answer in AnswersGiven)
        {
            //Increase the empty answers counter if the answer is empty
            if(answer == "")
            {
                emptyAnswers++;
            }
        }

        //If there is at least one answer
        if(emptyAnswers < 3)
        {
            //Deactivate the advance button
            transform.GetChild(3).GetComponent<Button>().interactable = false;

            //Deactivate option's interaction and set it to fade out if it's not connected to any image
            foreach(GameObject option in Options)
            {
                option.GetComponent<Button>().interactable = false;
                if(option.GetComponent<Type3Option>().ImageSelected == null)
                {
                    option.GetComponent<Animator>().SetBool("QuestionAnswered", true);
                }
            }

            for(int i = 0; i < 3; i++)
            {
                //Set each image to fade out
                transform.GetChild(1).GetChild(i).GetChild(0).GetComponent<Animator>().SetBool("QuestionAnswered", true);
            }

            //Set the advance button to fade out
            transform.GetChild(3).GetComponent<Animator>().SetBool("QuestionAnswered", true);

            //Deactivate option's interaction and set it to fade out if it's connected to an image
            foreach (GameObject option in Options)
            {
                option.GetComponent<Animator>().SetBool("QuestionAnswered", true);
            }

            //Format the images names as they are passed to the stats
            _stats.CreateType3(Images[0].name.Replace("t3_", ""), Images[1].name.Replace("t3_", ""), Images[2].name.Replace("t3_", ""), AnswersGiven[0], AnswersGiven[1], AnswersGiven[2]);

            Invoke("ShowNextScreen", 1f);
        }
        else
        {
            Debug.Log("No selected options.");
        }
    }

    new void OnEnable()
    {
        //Reset the answers given
        for(int i = 0; i < AnswersGiven.Length; i++)
        {
            AnswersGiven[i] = "";
        }

        //Reactivate the options
        StartCoroutine(ActivateOptions());
    }
}
