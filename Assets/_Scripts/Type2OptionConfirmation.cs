﻿using UnityEngine;
using UnityEngine.UI;

//THIS SCRIPT GOES INTO EVERY OPTION IN THE TYPE 2 SCREEN

public class Type2OptionConfirmation : MonoBehaviour
{
    //The answer's button
    private Button _button;

    //The answer's script
    private Type2Answer _type2AnswerScript;

    private void Start()
    {
        //Instance creation
        _type2AnswerScript = transform.parent.parent.GetComponent<Type2Answer>();
        _button = GetComponent<Button>();
    }

    public void SelectOption()
    {
        //Check if the answer is not currently selected
        if (!_type2AnswerScript.SelectedOptions.Contains(gameObject))
        {
            //Check (and handle) if the answer count is overflowing
            if (_type2AnswerScript.SelectedOptions.Count >= _type2AnswerScript.MaxSelectedAnswers)
            {
                //Reset the button's colours to its original colours
                Button firstIndexButton = _type2AnswerScript.SelectedOptions[0].GetComponent<Button>();
                ColorBlock firstIndexButtonCB = firstIndexButton.colors;
                firstIndexButtonCB.normalColor = AppColours.ButtonNormalColour;
                firstIndexButtonCB.selectedColor = AppColours.ButtonNormalColour;
                firstIndexButton.colors = firstIndexButtonCB;
                _type2AnswerScript.SelectedOptions[0].GetComponentInChildren<Text>().color = Color.black;

                //Remove the first selected option (of the currently selected ones)
                _type2AnswerScript.SelectedOptions.Remove(_type2AnswerScript.SelectedOptions[0]);
            }

            //Select the option and change its button colours
            _type2AnswerScript.SelectedOptions.Add(gameObject);
            ColorBlock cb = _button.colors;
            cb.normalColor = AppColours.ButtonSelectedColour;
            cb.selectedColor = AppColours.ButtonSelectedColour;
            _button.colors = cb;
            GetComponentInChildren<Text>().color = Color.white;
        }
        //If the answer is already selected
        else
        {
            //Remove the option and change its button colours back to default
            _type2AnswerScript.SelectedOptions.Remove(gameObject);
            ColorBlock cb = _button.colors;
            cb.normalColor = AppColours.ButtonNormalColour;
            cb.selectedColor = AppColours.ButtonNormalColour;
            _button.colors = cb;
            GetComponentInChildren<Text>().color = Color.black;
        }
    }

    public void ResetButtonColours()
    {
        //Reset the button's colours to its original colours
        _button = GetComponent<Button>();
        ColorBlock cb = _button.colors;
        cb.normalColor = AppColours.ButtonNormalColour;
        cb.selectedColor = AppColours.ButtonNormalColour;
        cb.disabledColor = new Color(0.784f, 0.784f, 0.784f);
        _button.colors = cb;
        GetComponentInChildren<Text>().color = Color.black;
    }
}
