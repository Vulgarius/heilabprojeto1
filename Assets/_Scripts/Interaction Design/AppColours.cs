﻿using UnityEngine;

public static class AppColours
{
    private static int ButtonAlt2SelectedOffset = 50;

    public static Color
        ButtonNormalColour = Color.white,

        ButtonSelectedColour = new Color((float) 41/255,
                                         (float) 128/255,
                                         (float) 185/255),

        ButtonAltSelectedColour = new Color((float) 41/255,
                                            (float) 128/255,
                                            (float) 185/255, 0.75f),

        ButtonAlt2SelectedColour = new Color((float) (41 + ButtonAlt2SelectedOffset)/255,
                                             (float) (128 + ButtonAlt2SelectedOffset)/255,
                                             (float) (185 + ButtonAlt2SelectedOffset)/255);
}
