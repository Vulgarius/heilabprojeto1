﻿using System.Collections;
using UnityEngine;

public class IntroductionScreenAdvanceActivation : MonoBehaviour
{
    //The time until the advance button is activated
    private float _timeToActivateOptions = 3f;

    // Start is called before the first frame update
    private void Awake()
    {
        StartCoroutine(ActivateOptions());
    }

    protected void OnEnable()
    {
        //Stop a hypothetic existing coroutine to avoid overlapping
        StopCoroutine(ActivateOptions());
        StartCoroutine(ActivateOptions());
    }

    private IEnumerator ActivateOptions()
    {
        //Deactivate the advance button
        transform.GetChild(1).gameObject.SetActive(false);

        //Wait x seconds before continuing
        yield return new WaitForSecondsRealtime(_timeToActivateOptions);

        //Activate the advance button
        transform.GetChild(1).gameObject.SetActive(true);
    }
}
