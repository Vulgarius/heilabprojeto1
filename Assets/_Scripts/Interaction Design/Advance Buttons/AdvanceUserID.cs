﻿using UnityEngine;
using UnityEngine.UI;

public class AdvanceUserID : GenericAdvanceButton
{
    //The user ID input box
    private InputField _userID;

    // Start is called before the first frame update
    void Start()
    {
        _userID = GameObject.FindGameObjectWithTag("UserInput").GetComponent<InputField>();
    }

    void LateUpdate()
    {
        //If the button is interactable
        if (_Button.interactable)
        {
            //If the answer is empty, reset colours to white
            if (_userID.text == "")
            {
                ChangeColourCannotAdvance();
            }
            //If the answer isn't empty, change colours to black
            else
            {
                if (_userID.text.Split(' ')[0] != "")
                {
                    ChangeColourCanAdvance();
                }
            }
        }
    }
}
