﻿public class AdvanceType1 : GenericAdvanceButton
{
    //The type 1 answer script
    private Type1Answer _answerScript;

    void Start()
    {
        //Instance creation
        _answerScript = transform.GetComponentInParent<Type1Answer>();
    }

    void LateUpdate()
    {
        //If the button is interactable
        if(_Button.interactable)
        {
            //If the answer is empty, reset colours to white
            if(_answerScript.SelectedAnswer == "")
            {
                ChangeColourCannotAdvance();
            }
            //If the answer isn't empty, change colours to black
            else
            {
                ChangeColourCanAdvance();
            }
        }
    }
}
