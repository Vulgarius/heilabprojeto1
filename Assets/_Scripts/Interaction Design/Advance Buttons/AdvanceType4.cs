﻿public class AdvanceType4 : GenericAdvanceButton
{
    //The type 4 answer script
    private Type4Answer _answerScript;

    void Start()
    {
        //Instance creation
        _answerScript = transform.GetComponentInParent<Type4Answer>();
    }

    void LateUpdate()
    {
        //If the button is interactable
        if(_Button.interactable)
        {
            //If the answer is empty, reset colours to white
            if(_answerScript.SelectedOption == null)
            {
                ChangeColourCannotAdvance();
            }
            //If the answer isn't empty, change colours to black
            else
            {
                ChangeColourCanAdvance();
            }
        }
    }
}
