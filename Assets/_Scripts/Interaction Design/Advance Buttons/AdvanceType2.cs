﻿public class AdvanceType2 : GenericAdvanceButton
{
    //The type 2 answer script
    private Type2Answer _answerScript;

    void Start()
    {
        //Instance creation
        _answerScript = transform.GetComponentInParent<Type2Answer>();
    }

    void LateUpdate()
    {
        //If the button is interactable
        if(_Button.interactable)
        {
            //If the answer is empty, reset colours to white
            if(_answerScript.SelectedOptions.Count == 0)
            {
                ChangeColourCannotAdvance();
            }
            //If the answer isn't empty, change colours to black
            else
            {
                ChangeColourCanAdvance();
            }
        }
    }
}
