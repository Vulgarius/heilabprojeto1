﻿public class AdvanceType3 : GenericAdvanceButton
{
    //The type 3 answer script
    private Type3Answer _answerScript;

    void Start()
    {
        //Instance creation
        _answerScript = transform.GetComponentInParent<Type3Answer>();
    }

    void LateUpdate()
    {
        //Used to test if the answer array is empty
        byte emptyAnswers = 0;
        foreach(string answer in _answerScript.AnswersGiven)
        {
            //Increase the empty answers counter if the answer is empty
            if(answer == "")
            {
                emptyAnswers++;
            }
        }

        //If the button is interactable
        if(_Button.interactable)
        {
            //If the answer is empty, reset colours to white
            if(emptyAnswers == 3)
            {
                ChangeColourCannotAdvance();
            }
            //If the answer isn't empty, change colours to black
            else
            {
                ChangeColourCanAdvance();
            }
        }
    }
}
