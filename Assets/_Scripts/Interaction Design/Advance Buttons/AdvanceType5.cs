﻿public class AdvanceType5 : GenericAdvanceButton
{
    //The type 5 answer script
    private Type5Answer _answerScript;

    void Start()
    {
        //Instance creation
        _answerScript = transform.GetComponentInParent<Type5Answer>();
    }

    void LateUpdate()
    {
        //If the button is interactable
        if(_Button.interactable)
        {
            //If the answer is empty, reset colours to white
            if(_answerScript.MatchedAnswers.Count == 0)
            {
                ChangeColourCannotAdvance();
            }
            //If the answer isn't empty, change colours to black
            else
            {
                ChangeColourCanAdvance();
            }
        }
    }
}
