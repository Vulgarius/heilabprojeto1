﻿using UnityEngine;
using UnityEngine.UI;

public class GenericAdvanceButton : MonoBehaviour
{
    //The advance button
    protected Button _Button;

    //The advance button's background image
    protected Image _Image;

    //The advance button's text
    protected Text _Text;

    //The advance button's arrow
    protected Image _Arrow;

    protected void Awake()
    {
        //Instance creation
        _Button = GetComponent<Button>();
        _Image = GetComponent<Image>();
        _Text = transform.GetChild(0).GetComponent<Text>();
        _Arrow = transform.GetChild(0).GetChild(0).GetComponentInChildren<Image>();

        ChangeColourCannotAdvance();
    }

    public void ChangeColourCanAdvance()
    {
        //Reset the advance button's colours
        ColorBlock currentAnswerCB = _Button.colors;
        currentAnswerCB.normalColor = Color.black;
        currentAnswerCB.selectedColor = Color.black;
        _Button.colors = currentAnswerCB;
        _Image.color = Color.black;
        _Text.color = Color.white;
        _Arrow.color = Color.white;
    }

    public void ChangeColourCannotAdvance()
    {
        //Reset the advance button's colours
        ColorBlock currentAnswerCB = _Button.colors;
        currentAnswerCB.normalColor += Color.white;
        currentAnswerCB.selectedColor += Color.white;
        _Button.colors = currentAnswerCB;
        _Image.color = Color.white;
        _Text.color = Color.black;
        _Arrow.color = Color.black;
    }

    protected void OnEnable()
    {
        ChangeColourCannotAdvance();
    }
}
