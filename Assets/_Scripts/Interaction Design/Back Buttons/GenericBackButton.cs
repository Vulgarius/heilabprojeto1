﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GenericBackButton : MonoBehaviour
{
    //This answer's type, should be: Type2, Type3, Type4, Type5
    public string AnswerType;

    public void GenericGoBack()
    {
        StopCoroutine("ShowPreviousScreen");
        StartCoroutine("ShowPreviousScreen");
    }

    private IEnumerator ShowPreviousScreen()
    {
        //Get the previous question's object
        GameObject previousQuestion;
        previousQuestion = transform.parent.parent.GetChild(transform.parent.GetSiblingIndex() - 1).gameObject;

        switch(AnswerType)
        {
            case "Type2":
                //Deactivate the back button
                previousQuestion.transform.GetChild(4).gameObject.SetActive(false);

                //Deactivate the advance button's interaction
                transform.parent.GetChild(3).GetComponent<Button>().interactable = false;

                //Deactivate every option's interaction and set it to fade out
                foreach (GameObject option in transform.GetComponentInParent<Type2Answer>().Options)
                {
                    //If the option is activated
                    if(option.activeSelf == true)
                    {
                        option.GetComponent<Animator>().SetBool("QuestionAnswered", true);
                    }
                }

                //Reactivate button interaction and reset button colours in the previous screen
                foreach(GameObject option in previousQuestion.GetComponent<Type2Answer>().Options)
                {
                    option.GetComponent<Button>().interactable = true;
                    option.GetComponent<Type2OptionConfirmation>().ResetButtonColours();
                }

                //Set the image do fade out
                transform.parent.GetChild(1).GetChild(0).GetComponent<Animator>().SetBool("QuestionAnswered", true);

                yield return new WaitForSeconds(1f);
                break;
            case "Type3":
                //Deactivate the back button
                previousQuestion.transform.GetChild(4).gameObject.SetActive(false);

                //Deactivate the advance button's interaction
                transform.parent.GetChild(3).GetComponent<Button>().interactable = false;

                foreach (GameObject option in transform.GetComponentInParent<Type3Answer>().Options)
                {
                    //If the option is activated
                    if(option.activeSelf == true)
                    {
                        option.GetComponent<Animator>().SetBool("QuestionAnswered", true);
                    }
                }

                for (int i = 0; i < 3; i++)
                {
                    //Set each image to fade out
                    transform.parent.GetChild(1).GetChild(i).GetChild(0).GetComponent<Animator>().SetBool("QuestionAnswered", true);
                }

                //Reactivate button interaction and reset button colours and position in the previous screen
                foreach (GameObject option in previousQuestion.GetComponent<Type3Answer>().Options)
                {
                    option.GetComponent<Button>().interactable = true;
                    option.GetComponent<Type3Option>().ResetButtonColours();
                    option.GetComponent<Type3Option>().ResetPosition();
                }
                yield return new WaitForSeconds(1f);
                break;
            case "Type4":
                //Deactivate the back button
                previousQuestion.transform.GetChild(4).gameObject.SetActive(false);

                //Deactivate the advance button's interaction
                transform.parent.GetChild(3).GetComponent<Button>().interactable = false;

                //Reactivate button interaction and reset button colours in the previous screen
                foreach (GameObject option in previousQuestion.GetComponent<Type4Answer>().Options)
                {
                    option.GetComponent<Button>().interactable = true;
                    option.GetComponent<Type4Option>().ResetButtonColours();
                }

                //Set both images to fade out
                transform.parent.GetChild(1).GetChild(0).GetChild(0).GetComponent<Animator>().SetBool("QuestionAnswered", true);
                transform.parent.GetChild(1).GetChild(1).GetChild(0).GetComponent<Animator>().SetBool("QuestionAnswered", true);

                yield return new WaitForSeconds(1f);
                break;
            case "Type5":
                //Deactivate the back button
                previousQuestion.transform.GetChild(3).gameObject.SetActive(false);

                //Deactivate the advance button's interaction
                transform.parent.GetChild(2).GetComponent<Button>().interactable = false;

                //Reactivate button interaction and reset button colours in the previous screen
                foreach (GameObject option in previousQuestion.GetComponent<Type5Answer>().Options)
                {
                    option.GetComponentInChildren<Button>().interactable = true;
                    option.GetComponentInChildren<Type5Option>().ResetButtonColours();
                }
                previousQuestion.transform.GetChild(2).GetComponent<Button>().interactable = true;
                previousQuestion.GetComponent<Type5Answer>().ResetScreen();

                for (int i = 0; i < 6; i++)
                {
                    //Set each image to fade out
                    transform.parent.GetChild(1).GetChild(i).GetComponent<Animator>().SetBool("QuestionAnswered", true);
                }

                yield return new WaitForSeconds(1f);
                break;
            default:
                break;
        }

        //Get the stats script
        StatsRegister stats = GameObject.FindGameObjectWithTag("Screens").GetComponent<StatsRegister>();

        //Decrease the current screen index to match the return
        GameObject.FindGameObjectWithTag("ScreenManager").GetComponent<ScreenManager>().CurrentScreenIndex--;

        //Remove the last answer given of this type
        stats.UndoAnswer(AnswerType);

        previousQuestion.SetActive(true);

        //Reactivate the advance button and reset its interaction
        //previousQuestion.transform.GetChild(3).gameObject.SetActive(true);
        if(AnswerType != "Type5")
        {
            previousQuestion.transform.GetChild(3).GetComponent<Button>().interactable = true;
            previousQuestion.transform.GetChild(3).GetComponent<Animator>().SetBool("QuestionAnswered", false);
        }
        else
        {
            previousQuestion.transform.GetChild(2).GetComponent<Button>().interactable = true;
            previousQuestion.transform.GetChild(2).GetComponent<Animator>().SetBool("QuestionAnswered", false);
        }

        //Deactivate the parent GameObject
        transform.parent.gameObject.SetActive(false);

        //Deactivate the back button
        gameObject.SetActive(false);

        //Update the progress bar
        GameObject.FindGameObjectWithTag("ProgressBar").GetComponent<ProgressBarBehaviour>().UpdateProgressBar();
    }
}
