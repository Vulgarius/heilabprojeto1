﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Type1BackButton : MonoBehaviour
{
    public void GoBack()
    {
        StopCoroutine("ShowPreviousScreen");
        StartCoroutine("ShowPreviousScreen");
    }

    private IEnumerator ShowPreviousScreen()
    {
        //Get the previous question's object
        GameObject previousQuestion;
        previousQuestion = transform.parent.parent.GetChild(transform.parent.GetSiblingIndex() - 1).gameObject;
        Type1Answer previousQuestionScript;
        previousQuestionScript = previousQuestion.GetComponent<Type1Answer>();

        //Deactivate the advance button's interaction
        transform.parent.GetChild(3).GetComponent<Button>().interactable = false;

        //Deactivate every option's interaction and set it to fade out
        foreach(GameObject option in transform.parent.GetComponent<Type1Answer>().Options)
        {
            //If the option is activated
            if(option.activeSelf == true)
            {
                option.GetComponent<Animator>().SetBool("QuestionAnswered", true);
            }
        }

        transform.parent.GetChild(1).GetChild(0).GetComponent<Animator>().SetBool("QuestionAnswered", true);

        //Get the stats script
        StatsRegister stats = GameObject.FindGameObjectWithTag("Screens").GetComponent<StatsRegister>();

        yield return new WaitForSeconds(1f);

        //Decrease the current screen index to match the return
        GameObject.FindGameObjectWithTag("ScreenManager").GetComponent<ScreenManager>().CurrentScreenIndex--;

        if(previousQuestion.GetComponent<Type1Answer>().IsCorrect)
        {
            //Check if the last answer was correct and remove its correct count if it was
            previousQuestion.GetComponent<Type1Answer>().IsCorrect = false;
            stats.CorrectT1--;

            //Reset the last screen to be moved out of the test
            if(GameObject.FindGameObjectWithTag("ScreenHolder").transform.childCount > 0)
            {
                GameObject.FindGameObjectWithTag("ScreenManager").GetComponent<ScreenManager>().ResetType1Reinforcement();
            }
        }

        //Activate the previous screen
        previousQuestion.SetActive(true);

        //Reset interaction and colour scheme on each of the options
        foreach(GameObject option in previousQuestionScript.Options)
        {
            option.GetComponent<Button>().interactable = true;
            option.GetComponent<Type1OptionConfirmation>().ResetButtonColours();
        }

        previousQuestion.transform.GetChild(3).GetComponent<Button>().interactable = true;
        previousQuestion.transform.GetChild(3).GetComponent<Animator>().SetBool("QuestionAnswered", false);
        
        previousQuestion.transform.GetChild(4).gameObject.SetActive(false);
        transform.parent.gameObject.SetActive(false);
        transform.parent.GetChild(4).gameObject.SetActive(false);

        //Update the progress bar
        GameObject.FindGameObjectWithTag("ProgressBar").GetComponent<ProgressBarBehaviour>().UpdateProgressBar();
    }
}
