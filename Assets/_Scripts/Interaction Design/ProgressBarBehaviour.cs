﻿using UnityEngine;

public class ProgressBarBehaviour : MonoBehaviour
{
    //The screen manager (from where we will get the screens list)
    private ScreenManager _screenManager;

    //The active screens parent
    private Transform _screens;

    [SerializeField]
    //The starting index for each type
    #region Question starting index
    private int
        _type1Start,
        _type2Start,
        _type3Start,
        _type4Start,
        _type5Start;
    #endregion

    [SerializeField]
    //The number of questions of each type
    #region Question numbers
    private int
        _type1NoOfQuestions,
        _type2NoOfQuestions,
        _type3NoOfQuestions,
        _type4NoOfQuestions,
        _type5NoOfQuestions;
    #endregion

    [SerializeField]
    //The progress percentage (for the current screen type)
    private float _percentage;

    void Awake()
    {
        //Instance creation
        _screenManager = GameObject.FindGameObjectWithTag("ScreenManager").GetComponent<ScreenManager>();
    }

    public void UpdateProgressBar()
    {
        //Reset the values
        #region Value reset
        _type1Start = 0;
        _type1NoOfQuestions = 0;
        _type2Start = 0;
        _type2NoOfQuestions = 0;
        _type3Start = 0;
        _type3NoOfQuestions = 0;
        _type4Start = 0;
        _type4NoOfQuestions = 0;
        _type5Start = 0;
        _type5NoOfQuestions = 0;
        #endregion

        foreach(GameObject screen in _screenManager._screens)
        {
            //Each name condition adds to the start position and number of questions of its type
            #region Name check if-tree
            //If the screen's name contains "ale" (from "female" and "male"), then it is type 1
            if(screen.name.Contains("ale"))
            {
                _type2Start++;
                _type1NoOfQuestions++;
            }
            //If the screen's name contains "Type 2 ", then it is type 2
            if(screen.name.Contains("Type 2 "))
            {
                _type3Start++;
                _type2NoOfQuestions++;
            }
            //If the screen's name contains "Type 3 ", then it is type 3
            if(screen.name.Contains("Type 3 "))
            {
                _type4Start++;
                _type3NoOfQuestions++;
            }
            //If the screen's name contains "Type 4 ", then it is type 4
            if(screen.name.Contains("Type 4 "))
            {
                _type5Start++;
                _type4NoOfQuestions++;
            }
            //If the screen's name contains "Type 5 ", then it is type 5
            if(screen.name.Contains("Type 5 "))
            {
                _type5NoOfQuestions++;
            }
            #endregion
        }

        //Increase the starting positions by each other
        #region Matryoshka party!
        _type2Start += _type1Start;
        _type3Start += _type2Start;
        _type4Start += _type3Start;
        _type5Start += _type4Start;
        #endregion

        //Calculate the percentage by sending the current type as a parameter
        #region Calculate progress percentage
        if(_screenManager.CurrentScreenIndex <= _type2Start)
        {
            CalculatePercentage(1);
        }
        else if(_screenManager.CurrentScreenIndex <= _type3Start)
        {
            CalculatePercentage(2);
        }
        else if(_screenManager.CurrentScreenIndex <= _type4Start)
        {
            CalculatePercentage(3);
        }
        else if(_screenManager.CurrentScreenIndex <= _type5Start)
        {
            CalculatePercentage(4);
        }
        else
        {
            CalculatePercentage(5);
        }
        #endregion
    }

    private void CalculatePercentage(int type)
    {
        //Calculate percentage by dividing the current screen index by the start of the next type (minus one to adjust to the end of this type)
        //19.55% is the percentage of each type in a balanced progress bar (0.45% is reserved for the space between each bar)
        switch (type)
        {
            case 1:
                _percentage = (float)_screenManager.CurrentScreenIndex / _type2Start * 0.1955f;
                RescaleProgressBar(0, _percentage);
                break;
            case 2:
                _percentage = (float)(_screenManager.CurrentScreenIndex - _type2Start) / (float)(_type3Start - _type2Start) * 0.1955f;
                RescaleProgressBar(1, _percentage);
                break;
            case 3:
                _percentage = ((float)_screenManager.CurrentScreenIndex - _type3Start) / (float)(_type4Start - _type3Start) * 0.1955f;
                RescaleProgressBar(2, _percentage);
                break;
            case 4:
                _percentage = ((float)_screenManager.CurrentScreenIndex - _type4Start) / (_type5Start - _type4Start) * 0.1955f;
                RescaleProgressBar(3, _percentage);
                break;
            case 5:
                _percentage = ((float)_screenManager.CurrentScreenIndex - _type5Start) / _type5NoOfQuestions * 0.1955f;
                RescaleProgressBar(4, _percentage);
                break;
            default:
                break;
        }
    }

    private void RescaleProgressBar(int barID, float percentage)
    {
        transform.GetChild(barID).GetChild(0).GetChild(0).GetComponent<RectTransform>().localScale = new Vector3(percentage, 1, 1);
    }
}