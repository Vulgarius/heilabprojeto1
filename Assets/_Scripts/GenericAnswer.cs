﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GenericAnswer : MonoBehaviour
{
    //The stats register script (to register the answer)
    protected StatsRegister _stats;
    
    //The time until the options show up on screen
    protected float _timeToActivateOptions = 3f;

    //Every option button's GameObject
    public GameObject[] Options;

    //The number of options available
    public byte OptionNumber;

    //The answer texts (to put in the options)
    public List<string>
        Answers = new List<string>(),
        tempAnswers = new List<string>();

    // Start is called before the first frame update
    protected void Awake()
    {
        //Instance creation
        _stats = transform.GetComponentInParent<StatsRegister>();

        //Reset option GameObjects
        Options = new GameObject[OptionNumber];
        //Fill it with the option buttons
        for (int i = 0; i < OptionNumber; i++)
        {
            Options[i] = transform.GetChild(2).GetChild(i).gameObject;
        }

        //Shuffle every answer
        ShuffleAnswers();

        StartCoroutine(ActivateOptions());
    }

    protected void OnEnable()
    {
        //Stop a possible active coroutine to avoid overlapping
        StopCoroutine(ActivateOptions());
        StartCoroutine(ActivateOptions());
    }

    protected void ShuffleAnswers()
    {
        //Create a temporary list with the answers
        for(int i = 0; i < OptionNumber; i++)
        {
            tempAnswers.Add(Answers[i]);
        }
        

        for(int i = 0; i < Options.Length; i++)
        {
            //Randomly generate a button index on which the answer will write itself
            int index = Random.Range(0, tempAnswers.Count);

            //Match the button's text to the answer text
            Options[i].GetComponentInChildren<Text>().text = tempAnswers[index];
            //Remove the answer text from the answers list
            tempAnswers.Remove(tempAnswers[index]);
        }
    }

    protected IEnumerator ActivateOptions()
    {
        //Deactivate the objects (to avoid inconsistency when the user presses the back button)
        ToggleObjectByChildIndex(2, false);
        ToggleObjectByChildIndex(3, false);

        //Wait x seconds before continuing
        yield return new WaitForSecondsRealtime(_timeToActivateOptions);
        
        //Activate the options parent object (which activates the options)
        ToggleObjectByChildIndex(2, true);

        //Wait x seconds before continuing
        yield return new WaitForSecondsRealtime(_timeToActivateOptions - 1f);

        //Activate the advance button
        if (!transform.GetChild(3).GetComponent<Animator>().GetBool("QuestionAnswered"))
        {
            ToggleObjectByChildIndex(3, true);
        }
    }

    protected void ConfirmAnswer()
    {
        Debug.Log("Confirming answer.");
    }

    protected void ShowNextScreen()
    {
        //Hide this screen and show the next one
        GameObject.FindGameObjectWithTag("ScreenManager").GetComponent<ScreenManager>().ShowNextScreen();

        //Update the progress bar
        GameObject.FindGameObjectWithTag("ProgressBar").GetComponent<ProgressBarBehaviour>().UpdateProgressBar();
    }

    public void ToggleObjectByChildIndex(int childIndex, bool activationBool)
    {
        //Activate or deactivate the target child of this object
        transform.GetChild(childIndex).gameObject.SetActive(activationBool);

        //If the target object is a button, set it to interactable
        if(transform.GetChild(childIndex).GetComponent<Button>() != null)
        {
            transform.GetChild(childIndex).GetComponent<Button>().interactable = true;
        }
    }
}
