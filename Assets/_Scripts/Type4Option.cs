﻿using UnityEngine;
using UnityEngine.UI;

public class Type4Option : MonoBehaviour
{
    //The parent answer script
    private Type4Answer _answerScript;

    //This answer's text
    public string ThisAnswer;

    //The other answer's text
    public Type4Option OtherAnswer;
    private string _otherAnswer;

    // Start is called before the first frame update
    void Start()
    {
        //Instance creation
        _answerScript = transform.parent.parent.GetComponent<Type4Answer>();
        _otherAnswer = OtherAnswer.ThisAnswer;
    }

    public void SelectOption()
    {
        //Check if the selected answer is different from this one
        if (_answerScript.SelectedOption != gameObject)
        {
            //If there is an answer already selected, reset its transparency
            if(_answerScript.SelectedOption != null)
            {
                //Change image transparency
                Image _selImage = _answerScript.SelectedOption.GetComponent<Image>();
                _selImage.color = new Color(0.2f, 0.2f, 0.2f, 0f);

                //Change button transparency
                Button _selButton = _answerScript.SelectedOption.GetComponent<Button>();
                ColorBlock _selButtonCB = _selButton.colors;
                _selButtonCB.normalColor = AppColours.ButtonNormalColour;
                _selButtonCB.selectedColor = AppColours.ButtonNormalColour;
                _selButton.colors = _selButtonCB;
            }

            _answerScript.SelectedOption = gameObject;

            //Change image transparency
            Image _image = GetComponent<Image>();
            _image.color = new Color(1f, 1f, 1f, 0.5f);

            //Change button transparency
            Button _button = GetComponent<Button>();
            ColorBlock _buttonCB = _button.colors;
            _buttonCB.normalColor = AppColours.ButtonAltSelectedColour;
            _buttonCB.selectedColor = AppColours.ButtonAltSelectedColour;
            _button.colors = _buttonCB;
        }
        //If it isn't, deselect this option
        else
        {
            _answerScript.SelectedOption = null;

            //Change image transparency
            Image _image = GetComponent<Image>();
            _image.color = new Color(0.2f, 0.2f, 0.2f, 0f);

            //Change button transparency
            Button _button = GetComponent<Button>();
            ColorBlock _buttonCB = _button.colors;
            _buttonCB.normalColor = AppColours.ButtonNormalColour;
            _buttonCB.selectedColor = AppColours.ButtonNormalColour;
            _button.colors = _buttonCB;
        }
    }

    public void ResetButtonColours()
    {
        //Change image transparency
        Image _image = GetComponent<Image>();
        _image.color = new Color(0.2f, 0.2f, 0.2f, 0f);

        //Change button transparency
        Button _button = GetComponent<Button>();
        ColorBlock _buttonCB = _button.colors;
        _buttonCB.normalColor = AppColours.ButtonNormalColour;
        _buttonCB.selectedColor = AppColours.ButtonNormalColour;
        _button.colors = _buttonCB;
    }

    void OnEnable()
    {
        ResetButtonColours();
    }
}
