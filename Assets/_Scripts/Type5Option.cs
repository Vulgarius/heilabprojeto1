﻿using UnityEngine;
using UnityEngine.UI;

public class Type5Option : MonoBehaviour
{
    //The parent answer script
    private Type5Answer _answerScript;

    //This answer's text
    public string Answer;

    // Start is called before the first frame update
    void Start()
    {
        //Instance creation
        _answerScript = transform.parent.parent.parent.GetComponent<Type5Answer>();
    }

    public void SelectOption()
    {
        //Check if any answer is already selected
        if(_answerScript.CurrentlySelectedAnswer != null)
        {
            //Check if the currently selected answer is not this one
            if(_answerScript.CurrentlySelectedAnswer != transform.parent.gameObject)
            {
                //Lock the selected answers
                _answerScript.MatchAnswers(_answerScript.CurrentlySelectedAnswer, transform.parent.gameObject);
            }
            //If it is, undo its selection
            else
            {
                //Change the button's colour back to default
                _answerScript.CurrentlySelectedAnswer.GetComponent<Image>().color = new Color(0.6509f, 0.6509f, 0.6509f);

                //Reset the currently selected answer
                _answerScript.CurrentlySelectedAnswer = null;
            }

            //Activate the advance button
            _answerScript.ToggleObjectByChildIndex(2, true);
        }
        //If not, select this answer
        else
        {
            _answerScript.CurrentlySelectedAnswer = transform.parent.gameObject;
            transform.parent.GetComponent<Image>().color = AppColours.ButtonAlt2SelectedColour;
        }
    }

    public void ResetButtonColours()
    {
        //Reset this button's colour
        transform.parent.GetComponent<Image>().color = new Color(0.6509f, 0.6509f, 0.6509f);
    }
}
